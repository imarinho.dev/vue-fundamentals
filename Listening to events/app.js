Vue.createApp({
	data() {
		return {
			age: 20,
		};
	},
	methods: {
		increment() {
			this.age++;
		},
		decrement() {
			this.age--;
		},
	},
}).mount('#app');
