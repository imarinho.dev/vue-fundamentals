Vue.createApp({
	data() {
		return {
			firstName: 'Iran',
			lastName: 'Marinho',
		};
	},
	methods: {
		fullName() {
			return `${this.firstName} ${this.lastName}`;
		},
	},
}).mount('#app');
