Vue.createApp({
	data() {
		return {
			firstName: 'Miro',
			middleName: '',
			lastName: 'Jack',
		};
	},
	methods: {
		fullName() {
			return `${this.firstName} ${this.middleName} ${this.lastName}`;
		},
		updateLastName(msg, event) {
			console.log(msg);
			this.lastName = event.target.value;
		},
		updateMiddleName(event) {
			this.middleName = event.target.value;
		},
	},
}).mount('#app');
