Vue.createApp({
	data() {
		return {
			firstName: 'Miro',
			lastName: 'Jack',
		};
	},
	methods: {
		updateLastName(msg, event) {
			console.log(msg);
			this.lastName = event.target.value;
		},
	},
}).mount('#app');
