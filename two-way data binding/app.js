Vue.createApp({
	data() {
		return {
			firstName: 'Naty',
			lastName: 'Castelo',
		};
	},
	methods: {
		fullName() {
			return `${this.firstName} ${this.lastName}`;
		},
	},
}).mount('#app');
